#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Tarjeta():

    def __init__(self, nombre, saldo):
        self.__nombre = nombre
        self.__saldo = saldo

    def get_nombre(self):
        return self.__nombre

    def get_saldo(self):
        return self.__saldo

    def set_saldo(self, saldo, s):
        # debe llegar un gasto menor o igual al del saldo
        if saldo > s:
            self.__saldo = saldo - s
        else:
            print("No tiene saldo suficiente para hacer la compra.")
