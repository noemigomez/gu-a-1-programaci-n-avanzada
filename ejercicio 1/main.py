#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Tarjeta import Tarjeta


# cambia saldo producto de una compra
def cambio_saldo(saldo1):
    try:
        opcion = str(input("¿Desea realizar una compra? (S/n): "))
        if opcion.upper() == "S":
            saldo2 = int(input("Inserte valor en USD: "))
            titular.set_saldo(saldo1, saldo2)
            print("Saldo de la tarjeta: {0} USD.".format(titular.get_saldo()))
        else:
            pass
    except ValueError:
        pass


if __name__ == "__main__":
    nombre = "Magnus Bane"
    saldo1 = 700

    # se crea el titular
    titular = Tarjeta(nombre, saldo1)

    print("El titular es {0}".format(titular.get_nombre()))
    print("El saldo de la tarjeta es {0} USD.".format(titular.get_saldo()))
    cambio_saldo(saldo1)
