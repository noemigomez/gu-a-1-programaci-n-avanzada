#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Biblioteca import Biblioteca


class Libro():

    def __init__(self, nombre, autor, ejemplares, ejemplares_prestados):
        self.nombre = nombre
        self.autor = autor
        self.__ejemplares = ejemplares
        self.__ejemplares_prestados = ejemplares_prestados
        # se asocia después
        self.biblioteca = None

    def asocia_biblioteca(self, biblioteca):
        if isinstance(biblioteca, Biblioteca):
            self.biblioteca = biblioteca

    def get_ejemplares(self):
        return self.__ejemplares

    def get_ejemplares_prestados(self):
        return self.__ejemplares_prestados

    def set_ejemplates_prestados(self, ejemplares_prestados):
        self.__ejemplares_prestados = ejemplares_prestados
