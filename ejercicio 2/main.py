#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Biblioteca import Biblioteca
from Libro import Libro


def imprimir(libro):
    print("LIBROS")
    # para imprimir el libro
    in_ = 1
    for i in libro:
        print("-----------------{0}-----------------".format(in_))
        print("Título: {0}".format(i.nombre))
        print("Autor: {0}".format(i.autor))
        print("Ejemplares: {0}".format(i.get_ejemplares()))
        print("Ejemplares prestados: {0}".format(i.get_ejemplares_prestados()))
        print("Biblioteca: {0}".format(i.biblioteca.nombre))
        in_ += 1


if __name__ == "__main__":
    b = Biblioteca("Biblioteca Noemí Gómez")
    # lista de los libros
    libro = []

    libro.append(Libro("Ciudad de Hueso", "Cassandra Clare", 3, 1))
    libro.append(Libro("It", "Stepehen King", 2, 2))
    libro.append(Libro("Mujercitas", "Louisa May Alcott", 4, 2))
    libro.append(Libro("Siega", "Neal Shusterman", 1, 0))
    libro.append(Libro("Los juegos del hambre", "Suzanne Collins", 2, 1))
    # se la biblioteca a cada libro
    for i in libro:
        i.asocia_biblioteca(b)
    imprimir(libro)
