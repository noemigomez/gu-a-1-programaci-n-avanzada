#!/usr/bin/env python3
# -*- coding:utf-8 -*-


def arreglo(list):
    for i in range(4):
        # si tiene más de un dígito, se toma la unidad (max: 36[9*4])
        if list[i] > 9:
            list[i] = list[i] % 10
    return list


class Codigo():

    def __init__(self, codigo):
        # se pasa a lista para trabajarlo
        lista = []
        for i in list(str(codigo)):
            lista.append(int(i))
        self.codigo = lista
        self.codigo1 = None
        self.codigo2 = None
        self.codigo3 = None

    def primer_cambio(self, codigo):
        codigo1 = []
        # hasta que formar 4 numeros
        while len(codigo1) != 4:
            codigo1.append(0)
            for i in range(len(codigo1)):
                # para sumar, no usar append
                codigo1[len(codigo1) - 1] += self.codigo[i]
        # revision para no tener ej: 10, sino 0
        codigo1 = arreglo(codigo1)
        self.codigo1 = codigo1

    def segundo_cambio(self, codigo1):
        codigo2 = []
        # sumar 7
        for i in self.codigo1:
            codigo2.append(i + 7)
        codigo2 = arreglo(codigo2)
        self.codigo2 = codigo2

    def tercer_cambio(self, codigo2):
        codigo3 = []
        # cambio de posicion
        for i in range(4):
            codigo3.append(self.codigo2[3 - i])
        self.codigo3 = codigo3

    def final(self, codigo3):
        final = ''
        print(self.codigo1, self.codigo2, self.codigo3)
        # de lista a codigo str
        for i in range(4):
            final += str(self.codigo3[i])
        self.__final = final

    def get_final(self):
        return self.__final
