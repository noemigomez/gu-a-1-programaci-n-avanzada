#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from Codigo import Codigo


if __name__ == "__main__":
    try:
        c = int(input("Ingrese código de 4 números: "))
        # codigo de 4 números
        if 10000 > c > 999:
            codigo = Codigo(c)
            codigo.primer_cambio(codigo)
            codigo.segundo_cambio(codigo.codigo1)
            codigo.tercer_cambio(codigo.codigo2)
            codigo.final(codigo.codigo3)

            confirm = str(input('¿Desea ver su código?: (S: si, otro: no):'))
            if confirm.upper() == 'S':
                print(codigo.get_final())
            else:
                print('Su código permanecerá secreto.')
        else:
            print('Eran 4 números para nuestro sistema super secreto.')

    except ValueError:
        print('Error de ingreso.')
