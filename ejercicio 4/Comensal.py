#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Comensal():

    def __init__(self, nombre, apellido, disponible):
        self.nombre = nombre
        self.apellido = apellido
        self.__disponible = disponible
        self.paga = 0

    def get_disponible(self):
        return self.__disponible

    def set_disponible(self, parte, dinero):
        self.__disponible = dinero - parte

    def loquepaga_santo(self, parte, total):
        # se obtiene el dinero disponible del santísimo
        dinero = self.get_disponible()
        # si tiene menos o igual de lo que hay que pagar
        if dinero <= parte:
            self.set_disponible(0, 0)
            self.paga = dinero
        else:
            self.set_disponible(parte, dinero)
            self.paga = parte
        # se resta al total lo pagado
        total += self.paga * -1

    # funcion que divide la cuenta entre los comensales
    def dividir_cuenta(self, amigos, parte, total):
        # cada amigo
        for i in range(len(amigos)):
            dinero = amigos[i].get_disponible()
            if dinero == parte:
                amigos[i].paga += parte
            elif dinero < parte:
                amigos[i].paga += dinero
                # para no quedar con billetera negativa
                dinero = parte
            else:
                amigos[i].paga += parte
            # actualizacion de dinero
            amigos[i].set_disponible(parte, dinero)
            total += amigos[i].paga * -1

        # si el total no se cubre
        if total > 0:
            return False, total
        else:
            return True, total
